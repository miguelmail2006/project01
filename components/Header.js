import React from 'react';
import { Link } from 'react-router-dom';

import SearchBar from './SearchBar'


const Header = () => {
    return (
        <div className="ui secondary pointing menu">
            <Link to="/" className="item">
                <button class="ui basic button">
                    <i class="home icon" />
                    Home
                </button>
            </Link>

            <Link to="/routes/Articles" className="item">
                <button class="ui basic button">
                    <i class="folder icon" />
                    Articles
                </button>
            </Link>

            <Link to="/routes/Media" className="item">
                <button class="ui basic button">
                    <i class="camera icon" />
                    Media
                </button>
            </Link>

            <div className="right menu">
                <SearchBar />
            </div>

        </div>
    )
}

export default Header;