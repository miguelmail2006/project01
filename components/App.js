import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Articles from './routes/Articles';
import Media from './routes/Media';
import Home from './routes/Home'
import Header from './Header'

const App = () => {
    return (
        <div className="ui container">
            <BrowserRouter>
                <div>
                    <Header />
                    <Route path="/" exact component={Home} />
                    <Route path="/routes/Articles" exact component={Articles} />
                    <Route path="/routes/Media" exact component={Media} />
                </div>
            </BrowserRouter>
        </div>
    )
}

export default App;